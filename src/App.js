
import React from 'react';
import './App.css';
import './reset/reset.css';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data_enter: false,
      data_notloaded:false,
      weatherdata: null,
      fetch_error: false,
      data_loaded:false
    }


  }

  getweather = (event) => {
    // show the user input value to console
    let userValue = event.target.value;

    console.log("user_value", userValue);
    const apiKey = '01e2a71059ec9f4d231db826f23b6f1c';

    console.log(this.state.city);
    const cityName = userValue;
    if(event.key == "Enter"){
      console.log("dataloading");
      this.setState({
        data_enter: true,
        find_nocity:false,
        weatherdata: null,
        fetch_error: false,
        data_loaded:false
      })
    }


    if (event.key == "Enter") {

      fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${cityName}&appid=${apiKey}&units=metric`)
        .then(response => response.json())
        .then(data => {
          console.log(data);
          if (data.cod == "200") {
            this.setState({
              find_nocity:false,
              data_enter: false,
              weatherdata: data.list,
              fetch_error: false,
              data_loaded:true

            })
          }
          else {
            this.setState({
              weatherdata: null,
              fetch_error: false,
              find_nocity:true,
              data_enter:false,
              data_loaded:false
            });
          }

          console.log(data);
        }).catch((err) => {
          console.log("hey", err);
          this.setState({
            weatherdata: null,
            fetch_error: true,
            find_nocity:false,
            data_enter:false,
            data_loaded:false

          });

        })

    }
  }
  render() {
    console.log(this.state.fetch_error);
    console.log(this.state.weatherdata);
    let finallist = this.state.weatherdata

    let resultArray = [];

    //date wise data 
    if (finallist) {
      const reducedData = finallist.reduce((dateData, eachData) => {
        let dateArray = eachData.dt_txt.split(" ");
        let date = dateArray[0];
        if (!(date in dateData)) {

          dateData[date] = eachData;
        }
        return dateData;
      }, {});
      console.log("reducedData", reducedData);

      //data according to date in a list 
      const finalData = Object.values(reducedData);

      console.log(finalData);

      
      resultArray=finalData.reduce((resultArray1,eachData)=>{
        let temp={};
        temp["day"] = (new Date((eachData.dt) * 1000).toLocaleString("en-us", { weekday: "long" }));

        temp["main"] = (eachData.weather[0].main);
        temp["weather-description"] = (eachData.weather[0].description);
        temp["weather-icon"] = (eachData.weather[0].icon);
        temp["max-temp"] = (eachData.main.temp_max);
        temp["min-temp"] = (eachData.main.temp_min);
        resultArray1.push(temp);
        return resultArray1;
      },[]);

      console.log(resultArray)
     
    }

  
  return(<div>
  <div className="container">
    <h1 id="weather_heading">Weather Forecasting</h1>
    <input className="input"
      placeholder="Enter City"
      onKeyPress={this.getweather}
      name="city"
      type="text"
      value={this.state.city}
      onChange={this.getweather}
    ></input>
    {(this.state.data_enter) && ( <div class="loader"></div> ) }
     {this.state.find_nocity && (<div>could not find the city</div>)}
    {this.state.fetch_error && (<div>could Not load Data</div>)}
    {this.state.weatherdata && (
      <div className="weatherDataContianer">
        {
          resultArray.map(eachDataDay => {
            return (
              <div className="eachDay">
                <div className="weather">{eachDataDay["day"]}</div>
                <div className="weather">{eachDataDay["main"]}</div>
                <div className="description">{eachDataDay["weather-description"]}</div>
                <div className="icon">{eachDataDay["weather-icon"] &&
                  <img src={`http://openweathermap.org/img/wn/${eachDataDay["weather-icon"]}.png`}></img>}</div>

                <div className="temperature">
                  <div className="temp_max">{eachDataDay["max-temp"]}&deg;</div>
                  <div className="temp_min">{eachDataDay["min-temp"]}&deg;</div>
                </div>
              </div>
            )
          })

        }
      </div>)
    }
  </div>
      </div >
    )
  }
}
export default App;